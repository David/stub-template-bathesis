Derived from Stefan Schweter's [Pandoc thesis template](http://fachschaft.cis.uni-muenchen.de/forum/showthread.php?tid=305&pid=841#pid841).

Based on Daniel Bruder's [typesetting template](https://github.com/xdbr/stub-template-typesetting).

Compatible with Daniel Bruder's [*stub* tool](https://github.com/xdbr/stub) templating tool.
