You can just download and overwrite this template.

Alternatively, let *stub* fill in the variables for you:

``` { .bash }
stub template:info template=git@gitlab.cis.uni-muenchen.de:David/stub-template-bathesis.git

stub template:new template=git@gitlab.cis.uni-muenchen.de:David/stub-template-bathesis.git to=bathesis name="NAME"
```


Installation (dependencies)
---------------------------

For z-shell users, use `~/.zshrc` instead of `~/.bashrc` where appropriate.

Some of the following installation routines assume the these prerequisites:

- Local directory for optional software: `mkdir -p ~/opt`
- Local directory for local binaries: `mkdir -p ~/bin`
- Extended PATH variable that includes local binaries: `echo 'PATH=~/bin:$PATH' >> ~/.bashrc`


### Pandoc (>= 1.12.3.3) & Citeproc

<http://www.johnmacfarlane.net/pandoc/>

<https://github.com/jgm/pandoc-citeproc/>

For Windows and Mac, go to the official Pandoc download page: <http://johnmacfarlane.net/pandoc/installing.html>

The recommended way is to install Pandoc (and pandoc-citeproc) locally, since its release cycle is very tight and the official repositories are most often outdated. First install the Haskell platform (<http://www.haskell.org/platform/>), then use the `cabal` tool:

``` { .bash }
cabal update
cabal install pandoc pandoc-citeproc
echo 'export PATH=$HOME/.cabal/bin:+$PATH' >> ~/.bashrc
source ~/.bashrc
pandoc --version # should be >= 1.12.3.3
```


### General Purpose Preprocessor (GPP)

<http://en.nothingisreal.com/wiki/GPP>

Follow the instructions on the webpage. On our CIP machines, GPP is already installed.


### Pandoc Preprocessor (PPP)

<https://github.com/xdbr/p5-App-pandoc-preprocess>

Download or clone the repository:

``` { .bash }
git clone https://github.com/xdbr/p5-App-pandoc-preprocess.git ~/opt/
```

Create a local symlink to the binary:

``` { .bash }
ln -s ~/opt/p5-App-pandoc-preprocess/bin/ppp ~/bin/ppp
```

Test:

``` { .bash }
which ppp
```


Installation (optional features)
--------------------------------


### Stub

<https://github.com/xdbr/stub>

Download:

``` { .bash }
curl --silent https://raw.github.com/xdbr/stub/master/install.sh | $SHELL
```

Create a local symlink to the binary:

``` { .bash }
ln -s ~/.stub/stub ~/bin/stub
```

Test:

``` { .bash }
stub -v
```


### Ditaa

Download:

``` { .bash }
mkdir -p ~/opt/ditaa && wget http://skylink.dl.sourceforge.net/project/ditaa/ditaa/0.9/ditaa0_9.zip -P ~/opt/ditaa/
unzip ~/opt/ditaa/ditaa0_9.zip
rm -f ~/opt/ditaa/ditaa0_9.zip
```

Setup the calling script:

``` { .bash }
echo '#!/bin/bash\nexec java -Dfile.encoding=UTF-8 -jar ~/opt/ditaa/ditaa0_9.jar "$@"' > ~/bin/ditaa && chmod a+x ~/bin/ditaa
```

Test:

``` { .bash }
ditaa -v
```


### PlantUML

<http://plantuml.sourceforge.net/>

Dependency: *rsvg-convert* (installed on our CIP machines)

Download:

``` { .bash }
mkdir -p ~/opt/plantuml && wget http://optimate.dl.sourceforge.net/project/plantuml/plantuml.jar -P ~/opt/plantuml/
```

Setup the calling script:

``` { .bash }
echo '#!/bin/bash\nexec java -Dfile.encoding=UTF-8 -jar ~/opt/plantuml/plantuml.jar "$@"' > ~/bin/plantuml && chmod a+x ~/bin/plantuml
```

Test:

``` { .bash }
plantuml -v
```


### Dot

Part of the *graphviz* package (installed on our CIP machines).


### Neato

Part of the *graphviz* package (installed on our CIP machines).

Dependency: *rsvg-convert* (installed on our CIP machines)
