Hiermit versichere ich, die vorliegende Arbeit selbstständig verfasst und keine
anderen als die angegebenen Quellen und Hilfsmittel benutzt sowie die Zitate
deutlich kenntlich gemacht zu haben.

München, den 26. Mai 2014

-------

{{name}}
