PPP supports several rendering engines (see [Installation]).

The following examples are taken from the PPP documentation at <https://github.com/xdbr/p5-App-pandoc-preprocess>.

They require the respected graph tools to be installed (see [Installation]).

## Ditaa

> A small utility that converts ascii-art diagrams to nice-looking bitmap diagrams automatically.

Project webpage: <http://ditaa.sourceforge.net/>

The following markup will produce Figure \ref{fig:pipeline-overview}.

\scriptsize

~~~~~ {.ditaa .rounded-corners .no-shadows .scale=90% .title=The ppp and pandoc pipeline .label=fig:pipeline-overview  .no-antialias .no-separation}
+-----------------+       +--------+           +--------------------+
| markdown source |------>| ppp    |------*--->| pröcessed markdown |
+-----------------+       +--------+      |    +--------------------+
                              |           \--->| image files        |
                    +------------------+       +--------------------+
                    | diagram creation |
                    +------------------+
                    | ditaa/dot/rdfdot |
                    +------------------+
~~~~~

\normalsize

As with tables, you can use the ASCII drawing tool (<http://asciiflow.com/>) to conveniently draw flowcharts.

Move this code block four spaces to the left to make it parseable:

~~~~~ {.ditaa .rounded-corners .no-shadows .scale=90% .title=A more complex ppp and pandoc pipeline. .label=fig:pipeline-overview  .no-antialias .no-separation}
+-----------------+       +--------+           +--------------------+      +--------------+           +----------+
| pandoc/markdown |------>| ppp    |------*--->| processed markdown |      |              |           |  final   |
|    source       |       |        |      |    |                    |      | pandoc       |  LaTeX    |          |
+-----------------+       +--------+      |    +--------------------+----->| typesetting  +---------->|   PDF    |
                              |           \--->| image files        |      |              |           |          |
                    +------------------+       +--------------------+      +-------+------+           +----------+
                    | diagram creation |                                     |     |
                    +------------------+                 +---------------------+   |  HTML   +----------+
                    | ditaa/dot/rdfdot |                 | many other formats  |   +-------->| Website  |
                    +------------------+                 |      possible!      |             +----------+
                                                         +---------------------+
~~~~~


## PlantUML

> PlantUMLis a component that allows to quickly write
- sequence diagram,
- use case diagram,
- class diagram,
- activity diagram,
- component diagram,
- state diagram
- object diagram
- wireframe graphical interface

Project webpage: <http://plantuml.sourceforge.net/>

The following markup produces figure \ref{fig:plantuml-example-1}.

Move this code block four spaces to the left to make it parseable:

\scriptsize

~~~~ {.plantuml .scale=60% .title=PlantUML Example 1 .label=fig:plantuml-example-1}
@startuml
scale 350 width
[*] --> NotShooting

state NotShooting {
  [*] --> Idle
  Idle --> Configuring : EvConfig
  Configuring --> Idle : EvConfig
}

state Configuring {
  [*] --> NewValueSelection
  NewValueSelection --> NewValuePreview : EvNewValue
  NewValuePreview --> NewValueSelection : EvNewValueRejected
  NewValuePreview --> NewValueSelection : EvNewValueSaved

  state NewValuePreview {
     State1 -> State2
  }

}
@enduml
~~~~

\normalsize
