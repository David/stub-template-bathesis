## References

Headers can be referenced like so: [Graphs].

Other references use LaTex syntax both for labeling and for referencing. We have one referencable image here \ref{fig:duck1} and a table here \ref{tab:multiline_table}.


## Citations

Citations use an external filter: *pandoc-citeproc* (see installation instructions in [Pandoc (>= 1.12.3.3) & Citeproc])

Use Bibtex citations from the ACM digital library: <http://dl.acm.org>

Put them in into the same `.bib` file. Example entry:

~~~ {#mycode .bibtex .numberLines }
@inproceedings{Strbac2012,
 author = {\v{S}trbac-Savi\'{c}, Svetlana and Toma\v{s}evi\'{c}, Milo},
 title = {Comparative Performance Evaluation of the AVL and Red-black Trees},
 booktitle = {Proceedings of the Fifth Balkan Conference in Informatics},
 series = {BCI '12},
 year = {2012},
 isbn = {978-1-4503-1240-0},
 location = {Novi Sad, Serbia},
 pages = {14--19},
 numpages = {6},
 url = {http://doi.acm.org/10.1145/2371316.2371320},
 doi = {10.1145/2371316.2371320},
 acmid = {2371320},
 publisher = {ACM},
 address = {New York, NY, USA},
 keywords = {AVL trees, binary search trees, red-black trees},
}
~~~

In your thesis, cite the references like so: [@Strbac2012]
