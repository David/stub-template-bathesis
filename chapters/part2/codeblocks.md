See the Pandoc documentation at <http://johnmacfarlane.net/pandoc/README.html#verbatim-code-blocks>.

In Perl:
^[Source <http://www.cip.ifi.lmu.de/~capsamun/Uebungen/uebung13.html>]:

~~~ {#mycode .perl .numberLines}
#!/usr/bin/perl
#Aufgabe 13 Rekursion.2
#Berechnet die Summe der Zahlen n + (n-1) + (n-2) + ... + 1

use strict;
use warnings;
use utf8;

{
my $wert;

print "Bitte geben Sie eine Zahl ein >>> ";
chomp($wert=<>);
print "Das Ergebnis lautet: ",&sum($wert),"\n";
}

sub sum($){
    my $zahl=shift @_;
    if ($zahl == 0){
        return 0;
    } else {
        return ($zahl+&sum($zahl-1));
    }
}
~~~

Starting numeration from custom number:

~~~ {#mycode .python .numberLines startFrom="161" }
class TestMorphologyFunctions(unittest.TestCase):

    def test_top_suffixes(self):
        temp_words = ['Aabc', 'Babc', 'Babc', 'Aabc', 'Cabf', 'Babf',
                  'Cabf', 'Aabh', 'Aabh', 'Aabi']

        top_suffixes_dict = top_suffixes(temp_words, 2)

        self.assertEqual(top_suffixes_dict["bi"], 1)
        self.assertEqual(top_suffixes_dict["bh"], 2)
        self.assertEqual(top_suffixes_dict["bf"], 3)
        self.assertEqual(top_suffixes_dict["bc"], 4)
~~~

Supported languages for highlighting:

```
actionscript, ada, apache, asn1, asp, awk, bash, bibtex, boo, c, changelog,
clojure, cmake, coffee, coldfusion, commonlisp, cpp, cs, css, curry, d,
diff, djangotemplate, doxygen, doxygenlua, dtd, eiffel, email, erlang,
fortran, fsharp, gnuassembler, go, haskell, haxe, html, ini, java, javadoc,
javascript, json, jsp, julia, latex, lex, literatecurry, literatehaskell,
lua, makefile, mandoc, markdown, matlab, maxima, metafont, mips, modelines,
modula2, modula3, monobasic, nasm, noweb, objectivec, objectivecpp, ocaml,
octave, pascal, perl, php, pike, postscript, prolog, python, r,
relaxngcompact, restructuredtext, rhtml, roff, ruby, rust, scala, scheme,
sci, sed, sgml, sql, sqlmysql, sqlpostgresql, tcl, texinfo, verilog, vhdl,
xml, xorg, xslt, xul, yacc, yaml
```
