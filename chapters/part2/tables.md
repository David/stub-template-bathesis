Different ways of creating tables are possible. The following examples are taken from the official Pandoc documentation at <http://johnmacfarlane.net/pandoc/README.html#tables>.


## Simple tables

  Right     Left     Center     Default
-------     ------ ----------   -------
     12     12        12            12
    123     123       123          123
      1     1          1             1

Table:  Demonstration of simple table syntax.


## Multiline tables

--------------------------------------------------------------------
 Centered   Default           Right Left
  Header    Aligned         Aligned Aligned
----------- ------- --------------- --------------------------------
   First    row                12.0 Example of a row that
                                    spans multiple lines.

  Second    row                 5.0 Here's another one. Note
                                    the blank line between
                                    rows.
--------------------------------------------------------------------

Table: Here's the caption. It, too, may span
multiple lines. \label{tab:multiline_table}


## Pipe tables

| Right | Left | Default | Center |
|------:|:-----|---------|:------:|
|   12  |  12  |    12   |    12  |
|  123  |  123 |   123   |   123  |
|    1  |    1 |     1   |     1  |

  : Demonstration of simple table syntax.

No need for alignment with pipe tables. This is especially useful for when you want to autogenerate tables via a script:

fruit| price
-----|-----:
apple|2.05
pear|1.37
orange|3.09


## Grid tables

Grid tables can be easily drawn with this ASCII drawing tool: <http://asciiflow.com/>

: Sample grid table.

+---------------+---------------+--------------------+
| Fruit         | Price         | Advantages         |
+===============+===============+====================+
| Bananas       | $1.34         | - built-in wrapper |
|               |               | - bright color     |
+---------------+---------------+--------------------+
| Oranges       | $2.10         | - cures scurvy     |
|               |               | - tasty            |
+---------------+---------------+--------------------+
