This template provides the basic structure of a bachelor thesis at CIS. The chapters in the main part contain some useful examples of the most important Pandoc features as well as links to the official documentation.

I suggest you make two copies of this template:

1. For your thesis, to be wildly changed according to your needs.
1. Unchanged, as reference for specific Pandoc features.
