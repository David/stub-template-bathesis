# Based on Daniel's typesetting template Makefile: https://github.com/xdbr/stub-template-typesetting

DOC ?= main.md
BIB ?= bibliography.bib
CSS ?= http://netdna.bootstrapcdn.com/bootswatch/3.1.1/yeti/bootstrap.min.css
TO ?= pdf
NUP ?= 2x1

# OUTPUT = $(patsubst %.md,%.$(TO),$(DOC))
DOCUMENTNAME = $(patsubst %.md,%,$(DOC))

default: check full
all: pdf tex mobi html book eco epub docx odt

check:
	@echo "\\n\\nChecking availability of required programs..."
	@sh -c 'for i in pandoc ppp ditaa yuml rdfdot gpp perl dot neato; do \
		echo Checking availability of required program $$i... $$( \
		which $$i 2>&1 >/dev/null && echo good.); \
	done'

clean:
	find ./out/ -type f -iname main.\* | grep -v -P bib\|update\|checks\|build\|md\|html\|pdf | xargs rm -rf

clobber:
	find ./out/ -type f -iname main.\* | grep -v -P bib\|update\|checks\|build\|md | xargs rm -rf

full pdf mobi epub odt docx: check $(BIB) $(DOC)
	@echo "\\n\\nStarting typesetting process to pdf..."
	time cat $(DOC) \
		| gpp \
	 		-x \
	 		-U "<#" ">" "\B" "|" ">" "<" ">" "#" "" \
	 		-DLATEX \
	 		-DPAGEREFS \
	 		-DMINITOC \
	 		-DMAKEINDEX \
	 		-DSIDENOTES \
	 		-DPARTS \
	 		-DABSTRACT \
	 		-DTOC \
		| ppp \
		| pandoc \
			--smart \
			--csl res/csl/ieee.csl \
			--standalone \
			--number-sections \
			--chapters \
			--toc-depth=2 \
			--bibliography $(BIB) \
			--latex-engine=xelatex \
			--include-before-body=res/cover.tex \
			--variable=lang:ngerman \
			--variable mainfont="Linux Libertine O" \
			--variable documentclass="scrartcl" \
	  		--variable fontsize="11pt,abstracton,a4paper" \
	  		--variable header-includes='\usepackage{urs}' \
			--variable header-includes='\setcounter{secnumdepth}{3}' \
			--variable header-includes='\usepackage{tikz}' \
			--variable header-includes='\usepackage{sidenotes}' \
			--variable header-includes='\usepackage{makeidx}' \
			--variable header-includes='\usepackage{minitoc}' \
			--variable header-includes='\usepackage{ngerman}' \
			--variable header-includes='\makeindex' \
			--variable include-before='\dominitoc' \
			--variable documentclass=book \
			--output out/$(DOCUMENTNAME).$@
			# --toc \

ppp: check $(BIB) $(DOC)
	@echo "\\n\\nStarting typesetting process up to ppp stage..."
	time cat $(DOC) \
		| gpp \
	 		-x \
	 		-U "<#" ">" "\B" "|" ">" "<" ">" "#" "" \
	 		-DLATEX \
	 		-DPAGEREFS \
	 		-DMINITOC \
	 		-DMAKEINDEX \
	 		-DSIDENOTES \
	 		-DPARTS \
	 		-DABSTRACT \
	 		-DTOC \
		| ppp

tex: check $(BIB) $(DOC)
	@echo "\\n\\nStarting typesetting process to pdf..."
	time cat $(DOC) \
		| gpp \
	 		-x \
	 		-U "<#" ">" "\B" "|" ">" "<" ">" "#" "" \
	 		-DLATEX \
	 		-DPAGEREFS \
	 		-DMINITOC \
	 		-DMAKEINDEX \
	 		-DSIDENOTES \
	 		-DPARTS \
	 		-DABSTRACT \
	 		-DTOC \
	 	| ppp \
		| pandoc \
			--smart \
			--csl res/csl/ieee.csl \
			--standalone \
			--number-sections \
			--chapters \
			--toc-depth=2 \
			--bibliography $(BIB) \
			--latex-engine=xelatex \
			--include-before-body=res/cover.tex \
			--variable=lang:ngerman \
			--variable mainfont="Linux Libertine O" \
			--variable documentclass="scrartcl" \
	  		--variable fontsize="11pt,abstracton,a4paper" \
	  		--variable header-includes='\usepackage{urs}' \
			--variable header-includes='\setcounter{secnumdepth}{3}' \
			--variable header-includes='\usepackage{tikz}' \
			--variable header-includes='\usepackage{sidenotes}' \
			--variable header-includes='\usepackage{makeidx}' \
			--variable header-includes='\usepackage{minitoc}' \
			--variable header-includes='\usepackage{ngerman}' \
			--variable header-includes='\makeindex' \
			--variable include-before='\dominitoc' \
			--variable documentclass=book \
			--output out/$(DOCUMENTNAME).tex
			# --toc \
		# && \
		# 	latexmk -pdf $(DOCUMENTNAME)

book: pdf
	pdfbook out/$(DOCUMENTNAME).pdf --outfile out/$(DOCUMENTNAME)-book.pdf

eco: pdf
	pdfjam --nup $(NUP) --a4paper --landscape out/$(DOCUMENTNAME).pdf --outfile out/$(DOCUMENTNAME)-$(NUP)-eco.pdf

html: check
	@echo "\\n\\nStarting typesetting process to html..."
	time cat $(DOC) \
		| gpp \
	 		-x \
	 		-U "<#" ">" "\B" "|" ">" "<" ">" "#" "" \
	 		-DLATEX \
	 		-DPAGEREFS \
	 		-DMINITOC \
	 		-DMAKEINDEX \
	 		-DSIDENOTES \
	 		-DPARTS \
	 		-DABSTRACT \
	 		-DTOC \
		| ppp \
		| pandoc \
			--smart \
			--csl res/csl/ieee.csl \
			--standalone \
			--number-sections \
			--chapters \
			--toc-depth=2 \
			--bibliography $(BIB) \
			--latex-engine=xelatex \
			--include-before-body=res/cover.tex \
			--toc \
			--variable lang=ngerman \
			--variable include-before='<div class="container">' \
			--variable include-after='</div>' \
			--css $(CSS) \
			--output out/$(DOCUMENTNAME).html
	perl -i -pe 's{ppp\-.*?\-.*?/}{}g' out/$(DOCUMENTNAME).html

$(BIB):
	perl -i -pe 's{\|}{\/}g' $(BIB)


# --variable mainfont="Linux Libertine O" \
# --variable include-after='\deftranslation[to=German]{Acronyms}{Abkürzungsverzeichnis}\printglossary[type=\acronymtype,style=long]' \
