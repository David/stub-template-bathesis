stub-template-bathesis
======================

Pandoc project template for Bachelor theses.

Version 1/2014


Acknowledgments
---------------

Derived from Stefan Schweter's [Pandoc thesis template](http://fachschaft.cis.uni-muenchen.de/forum/showthread.php?tid=305&pid=841#pid841).

Based on Daniel Bruder's [typesetting template](https://github.com/xdbr/stub-template-typesetting).

Compatible with Daniel Bruder's [*stub* tool](https://github.com/xdbr/stub) templating tool.


Content
-------

Files/ directories you most likely want to edit:

- `main.md`: main Pandoc file with the general thesis structure (includes the files in `chapters/`)
- `chapters/*`: directory with the included chapters as single files
- `bibliography.bib`: BibTex file
- `res/*`: various resource files, like images

Files you may want to edit:

- `Makefile`: makefile for the compile directives
- `res/gpp-defines.gpp`: makro definition file for LaTex commands, used by the Pandoc Preprocessor (PPP)

Files you should not edit:

- `README.md`: this readme
- `out/*`: directory with the compiled output files (PDF, TEX, HTML)
- `res/csl/*`: BibTex style files
- `*.sty`: LaTex style files (you can tuck them into a subdirectory by setting the `TEXINPUTS` variable in your shell accordingly, or just install them globally)
- `project.json`: project configuration file for *stub* (can be deleted after downloading the template)


Installation
------------

You can just download and overwrite this template.

Alternatively, let *stub* fill in the variables for you:

``` { .bash }
stub template:info template=git@gitlab.cis.uni-muenchen.de:David/stub-template-bathesis.git

stub template:new template=git@gitlab.cis.uni-muenchen.de:David/stub-template-bathesis.git to=bathesis name="NAME"
```


Installation (dependencies)
---------------------------

For z-shell users, use `~/.zshrc` instead of `~/.bashrc` where appropriate.

Some of the following installation routines assume these prerequisites:

- Local directory for optional software: `mkdir -p ~/opt`
- Local directory for local binaries: `mkdir -p ~/bin`
- Extended PATH variable that includes local binaries: `echo 'PATH=~/bin:$PATH' >> ~/.bashrc`


### Pandoc (>= 1.12.3.3) & pandoc-citeproc

<http://www.johnmacfarlane.net/pandoc/>

<https://github.com/jgm/pandoc-citeproc/>

For Windows and Mac, go to the official Pandoc download page: <http://johnmacfarlane.net/pandoc/installing.html>

The recommended way is to install Pandoc (and pandoc-citeproc) locally, since its release cycle is very tight and the official repositories are most often outdated. First install the Haskell platform (<http://www.haskell.org/platform/>), then use the `cabal` tool:

``` { .bash }
cabal update
cabal install pandoc pandoc-citeproc
echo 'export PATH=$HOME/.cabal/bin:+$PATH' >> ~/.bashrc
source ~/.bashrc
pandoc --version # should be >= 1.12.3.3
```


### General Purpose Preprocessor (GPP)

<http://en.nothingisreal.com/wiki/GPP>

Follow the instructions on the webpage. On our CIP machines, GPP is already installed.


### Pandoc Preprocessor (PPP)

<https://github.com/xdbr/p5-App-pandoc-preprocess>

Download or clone the repository:

``` { .bash }
git clone https://github.com/xdbr/p5-App-pandoc-preprocess.git ~/opt/p5-App-pandoc-preprocess
```

Create a local symlink to the binary:

``` { .bash }
ln -s ~/opt/p5-App-pandoc-preprocess/bin/ppp ~/bin/ppp
```

Test:

``` { .bash }
which ppp
```


### XeLaTex

<http://www.texts.io/download/>

<http://www.tug.org/mactex/>


Installation (optional features)
--------------------------------


### Stub

<https://github.com/xdbr/stub>

Download:

``` { .bash }
curl --silent https://raw.github.com/xdbr/stub/master/install.sh | $SHELL
```

Create a local symlink to the binary:

``` { .bash }
ln -s ~/.stub/stub ~/bin/stub
```

Test:

``` { .bash }
stub -v
```


### Ditaa

Download:

``` { .bash }
mkdir -p ~/opt/ditaa && wget http://skylink.dl.sourceforge.net/project/ditaa/ditaa/0.9/ditaa0_9.zip -P ~/opt/ditaa/
unzip ~/opt/ditaa/ditaa0_9.zip -d ~/opt/ditaa/
rm -f ~/opt/ditaa/ditaa0_9.zip
```

Setup the calling script:

``` { .bash }
echo -e '#!/bin/bash\nexec java -Dfile.encoding=UTF-8 -jar ~/opt/ditaa/ditaa0_9.jar "$@"' > ~/bin/ditaa && chmod a+x ~/bin/ditaa
```

Test:

``` { .bash }
ditaa -v
```


### PlantUML

<http://plantuml.sourceforge.net/>

Dependency: *rsvg-convert* (installed on our CIP machines)

Download:

``` { .bash }
mkdir -p ~/opt/plantuml && wget http://optimate.dl.sourceforge.net/project/plantuml/plantuml.jar -P ~/opt/plantuml/
```

Setup the calling script:

``` { .bash }
echo -e '#!/bin/bash\nexec java -Dfile.encoding=UTF-8 -jar ~/opt/plantuml/plantuml.jar "$@"' > ~/bin/plantuml && chmod a+x ~/bin/plantuml
```

Test:

``` { .bash }
plantuml -v
```


### Dot

Part of the *graphviz* package (installed on our CIP machines).


### Neato

Part of the *graphviz* package (installed on our CIP machines).

Dependency: *rsvg-convert* (installed on our CIP machines)


Usage
-----

All typesetting commands are implemented via make.

### Basic commands

#### Output formats

Pandoc to PDF:


``` { .bash }
make
```

or

``` { .bash }
make pdf
```

Pandoc to printable PDF book:


``` { .bash }
make book
```

Pandoc to HTML does not require Latex and is perfect for quick drafts. Note that certain Latex-specific packages won't work here (like tikz graphs):


``` { .bash }
make html
```

Pandoc to raw TEX if you need it for inspection and debugging:


``` { .bash }
make tex
```

Other formats (just try them out, no guarantee):

```
mobi book eco epub docx odt
```

#### Tidying up

Clean the working directory from all intermediate output files, like the LaTex meta files:

``` { .bash }
make clean
```

Clean the working directory from all output files, including PDF and HTML:

``` { .bash }
make clobber
```

#### Other

Spelling correction (with `en_US` for English respectively):

``` { .bash }
aspell -H -d de_DE -c main.md
```

### Optional parameters

`make` accepts the following parameters:

- `DOC` (=main.md):
- `BIB` (=bibliography.bib)
- `CSS` (=http://netdna.bootstrapcdn.com/bootswatch/3.1.1/yeti/bootstrap.min.css)

Set one or more parameters by changing the `Makefile` or via command line:

``` { .bash }
make html DOC=main.md BIB=bibliography.bib CSS=http://netdna.bootstrapcdn.com/bootswatch/3.1.1/darkly/bootstrap.min.css
```

You can find a selection of CSS styles here:

<http://www.bootstrapcdn.com/#bootswatch_tab>


Installation (container virtualization)
---------------------------------------

Instead of building up the template on CIP, it can be build in a kind 
of virtual container with *docker*. For that purpose a ```Dockerfile```
was added. Just build the *docker* image with:

``` { .bash }
sudo docker build --rm -t bathesis:latest .
```

And wait until all dependencies have been downloaded and installed. 
After that command finishes, just run:

``` { .bash }
docker run -i -t -v `pwd`:/mnt/bathesis -w /mnt/bathesis bathesis:latest
```

And you can start the image and you can start the build process in 
that virtual container. Inside that container you can type ```make pdf``` 
and your bachelor thesis is going to be generated. Outside the container
you can do all your changes to the bachelor thesis files.

More information on docker can be found [here](https://docs.docker.com/).

Author
------

David Kaumanns
Center for Information and Language Processing
Ludwig Maximilians University Munich
<david@cis.lmu.de>


License
-------

Creative Commons Attribution 4.0 International License
