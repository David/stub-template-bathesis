<#include res/gpp-defines.gpp>

<!-- empty page -->
\newpage
\thispagestyle{empty}
\mbox{}

\pagenumbering{gobble}

# Erklärung der Selbstständigkeit {.unnumbered}

<#include chapters/000-statement.md>

# Kurzfassung {.unnumbered}

<#include chapters/000-abstract.md>

# Aufgabenstellung {.unnumbered}

<#include chapters/000-task.md>

# Danksagung {.unnumbered}

<#include chapters/000-acknowledgments.md>

\pagenumbering{arabic}

<#tableofcontents >

# Einleitung

<#include chapters/100-introduction.md>

# Verwandte Arbeiten

<#include chapters/200-related.md>

<!-- MAIN PART -->

<#part General stuff>

<#include chapters/300-part1.md>

<#part Pandoc examples>

<#include chapters/400-part2.md>

<!-- END MAIN PART -->

# Schluss

<#include chapters/500-conclusion.md>

\newpage

<#part Anhang>

<#appendix>

\listoffigures

\listoftables

\renewcommand{\indexname}{Index}
\addcontentsline{toc}{section}{Index}

\newpage
\printindex

# Literaturverzeichnis {.unnumbered}

<!-- references will be automatically created here -->
